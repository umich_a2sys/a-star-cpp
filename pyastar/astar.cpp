#include <queue>
#include <limits>
#include <cmath>
#include <cstddef>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <algorithm>


#ifdef _WIN32
    #ifdef shared_EXPORTS
        #define SHARED_EXPORT __declspec( dllexport )
    #else
        #define SHARED_EXPORT __declspec( dllexport )
    #endif
#else
    #define SHARED_EXPORT
#endif



// # IMPORTANT
// # The images are formatted as with dimensions (x,y,z) or (row, col, depth) and 
// # are C-Contiguous arrays! Therefore z has the fastest index, then y, then x
// # See this https://eli.thegreenplace.net/2015/memory-layout-of-multi-dimensional-arrays

#define DEBUG 0
#define NUM_NEIGHBORS 26
#define LARGE_NUMBER 1.0
#define RES 2.0
#define W0 1.0
// NEIGHBOR CUBE DISTANCES
#define ST 1.0  
#define DG1 1.4142135   // root 2
#define DG2 1.7320508   // root 3

struct NB {
  int idx;
  float m;
  NB(int a, float b) : idx(a), m(b) {}
  NB() : idx(-1), m(0.0) {}
};

// represents a single pixel
class Node {
  public:
    int idx;     // index in the flattened grid
    float cost;  // cost of traversing this pixel
    Node(int i, float c) : idx(i), cost(c) {}
};

// the top of the priority queue is the greatest element by default,
// but we want the smallest, so flip the sign
bool operator<(const Node &n1, const Node &n2) {
  return n1.cost > n2.cost;
}

bool operator==(const Node &n1, const Node &n2) {
  return n1.idx == n2.idx;
}

// See for various grid heuristics:
// http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#S7
// L_\inf norm (diagonal distance)
float linf_norm(int i0, int j0, int k0, int i1, int j1, int k1) {
  return std::max(std::max(std::abs(i0 - i1), std::abs(j0 - j1)), std::abs(k0 - k1));
}

// L_1 norm (manhattan distance)
float l1_norm(int i0, int j0, int k0, int i1, int j1, int k1) {
  return std::abs(i0 - i1) + std::abs(j0 - j1) + std::abs(k0 - k1);
}
// L_2 norm (euclidean distance)
float l2_norm(int i0, int j0, int k0, int i1, int j1, int k1) {
  return RES * std::sqrt(std::pow(std::abs(i0 - i1), 2) + std::pow(std::abs(j0 - j1), 2) + std::pow(std::abs(k0 - k1), 2));
}

// inline float octile(int i0, int j0, int k0, int i1, int j1, int k1) {
//   int distArray[3] = {std::abs(i0 - i1), std::abs(j0 - j1), std::abs(k0 - k1)};
//   std::sort(distArray, distArray + 3);
//   return RES * (distArray[0] * ST + (DG1 - ST) * distArray[1] + (DG2 - DG1) * distArray[2]);
// }
// octile norm
float octile(int i0, int j0, int k0, int i1, int j1, int k1) {
  int a = std::abs(i0 - i1), b = std::abs(j0 - j1) , c = std::abs(k0 - k1);
  if (a >= b) {
    if (b >= c) {
      return RES * (a * ST + (DG1 - ST) * b + (DG2 - DG1) * c);
    } else {
      if (a >= c) {
        return RES * (a * ST + (DG1 - ST) * c + (DG2 - DG1) * b); 
      } else {
        return RES * (c * ST + (DG1 - ST) * a + (DG2 - DG1) * b); 
      }
    }
  } 
  
  // b is greater than a
  if (a >= c) {
    return RES * (b * ST + (DG1 - ST) * a + (DG2 - DG1) * c);
  } else {
    if (b >= c) {
      // b is greater than a, c is greater than a, and b is greater than c
      return RES * (b * ST + (DG1 - ST) * c + (DG2 - DG1) * a); 
    } else {
      return RES * (c * ST + (DG1 - ST) * b + (DG2 - DG1) * a); 
    }
  }

  return RES * (c * ST + (DG1 - ST) * a + (DG2 - DG1) * b);
}

inline int get_index(int i, int j, int k, int w, int d) {
  return k + d * (j + w * i);
}


void printArray(struct NB data[], int length)
{
    for(int i(0); i < length; ++i)
    {
        std::cout << data[i].idx << ' ';
    }
    std::cout << std::endl;
}



extern "C" SHARED_EXPORT float astar(
      const float* weights, const int h, const int w, const int d,
      const int start, const int goal, bool diag_ok,
      int* paths);

// weights:        flattened h x w grid of costs
// h, w:           height and width of grid
// start, goal:    index of start/goal in flattened grid
// diag_ok:        if true, allows diagonal moves (8-conn.)
// paths (output): for each node, stores previous node in path

extern "C" SHARED_EXPORT float astar(
      const float* weights, const int h, const int w, const int d,
      const int start, const int goal, bool diag_ok,
      int* paths) {

  const float INF = std::numeric_limits<float>::infinity();

  // row, col, and depth indexes of goal
  int row_g = goal / (w *d);
  int col_g = (goal - (w * d * row_g)) / d;
  int depth_g = (goal - (w * d * row_g)) % d;

  Node start_node(start, 0.);
  Node goal_node(goal,  0.);

  float* costs = new float[h * w * d];
  for (int i = 0; i < h * w * d; ++i)
    costs[i] = INF;
  costs[start] = 0.;

  std::priority_queue<Node> nodes_to_visit;
  nodes_to_visit.push(start_node);

  NB* nbrs = new NB[NUM_NEIGHBORS];

  bool solution_found = false;
  while (!nodes_to_visit.empty()) {
    // .top() doesn't actually remove the node
    Node cur = nodes_to_visit.top();

    if (cur == goal_node) {
      goal_node.cost = cur.cost;
      solution_found = true;
      break;
    }

    nodes_to_visit.pop();
    // row,col,depth indexes of current node
    int row = cur.idx / (w *d);
    int col = (cur.idx - (w * d * row)) / d;
    int depth = (cur.idx - (w * d * row)) % d;


    if(DEBUG) {
      printf ("Expanding Node: idx = %d; (%d, %d, %d)\n", cur.idx, row, col, depth);
    }
    // check bounds and find up to eight neighbors: top to bottom, left to right
    // Middle of Cube
    nbrs[0] = (diag_ok && row > 0 && col > 0)          ? NB(get_index(row - 1, col - 1, depth, w, d), DG1)    : NB(-1, 0.0); // -Y -X
    nbrs[1] = (row > 0)                                ? NB(get_index(row - 1, col, depth, w, d), ST)         : NB(-1, 0.0); // -Y
    nbrs[2] = (diag_ok && row > 0 && col + 1 < w)      ? NB(get_index(row - 1, col + 1, depth, w, d), DG1)    : NB(-1, 0.0); // -Y +X
    nbrs[3] = (col > 0)                                ? NB(get_index(row, col - 1, depth, w, d), ST)         : NB(-1, 0.0); // -X
    nbrs[4] = (col + 1 < w)                            ? NB(get_index(row, col + 1, depth, w, d), ST)         : NB(-1, 0.0); // +X
    nbrs[5] = (diag_ok && row + 1 < h && col > 0)      ? NB(get_index(row + 1, col - 1, depth, w, d), DG1)    : NB(-1, 0.0); // +Y -X
    nbrs[6] = (row + 1 < h)                            ? NB(get_index(row + 1, col, depth, w, d), ST)         : NB(-1, 0.0); // +Y
    nbrs[7] = (diag_ok && row + 1 < h && col + 1 < w ) ? NB(get_index(row + 1, col + 1, depth, w, d), DG1)    : NB(-1, 0.0); // +Y +X
    // Top of Cube
    nbrs[8] = (diag_ok && row > 0 && col > 0 && depth + 1 < d)          ? NB(get_index(row - 1, col - 1, depth + 1, w, d), DG2)  : NB(-1, 0.0); // -Y -X +Z
    nbrs[9] = (row > 0 && depth + 1 < d)                                ? NB(get_index(row - 1, col, depth + 1, w, d), DG1)      : NB(-1, 0.0); // -Y +Z
    nbrs[10] = (diag_ok && row > 0 && col + 1 < w && depth + 1 < d)     ? NB(get_index(row - 1, col + 1, depth + 1, w, d), DG2)  : NB(-1, 0.0); // -Y +X+Z
    nbrs[11] = (col > 0 && depth + 1 < d)                               ? NB(get_index(row, col - 1, depth + 1, w, d), DG1)      : NB(-1, 0.0); // -X +Z
    nbrs[12] = (depth + 1 < d)                                          ? NB(get_index(row, col, depth + 1, w, d), ST)           : NB(-1, 0.0); // +Z
    nbrs[13] = (col + 1 < w && depth + 1 < d)                           ? NB(get_index(row, col + 1, depth + 1, w, d), DG1)      : NB(-1, 0.0); // +X +Z
    nbrs[14] = (diag_ok && row + 1 < h && col > 0 && depth + 1 < d)     ? NB(get_index(row + 1, col - 1, depth + 1, w, d), DG2)  : NB(-1, 0.0); // +Y -X +Z
    nbrs[15] = (row + 1 < h && depth + 1 < d)                           ? NB(get_index(row + 1, col, depth + 1, w, d), DG1)      : NB(-1, 0.0); // +Y +Z
    nbrs[16] = (diag_ok && row + 1 < h && col + 1 < w && depth + 1 < d) ? NB(get_index(row + 1, col + 1, depth + 1, w, d), DG2)  : NB(-1, 0.0); // +Y +X +Z

    // Bottom of Cube
    nbrs[17] = (diag_ok && row > 0 && col > 0 && depth > 0)         ? NB(get_index(row - 1, col - 1, depth - 1, w, d), DG2)  : NB(-1, 0.0); // -Y -X -Z
    nbrs[18] = (row > 0 && depth > 0)                               ? NB(get_index(row - 1, col, depth - 1, w, d), DG1)      : NB(-1, 0.0); // -Y -Z
    nbrs[19] = (diag_ok && row > 0 && col + 1 < w && depth > 0)     ? NB(get_index(row - 1, col + 1, depth - 1, w, d), DG2)  : NB(-1, 0.0); // -Y +X-Z
    nbrs[20] = (col > 0 && depth > 0)                               ? NB(get_index(row, col - 1, depth - 1, w, d), DG1)      : NB(-1, 0.0); // -X -Z
    nbrs[21] = (depth > 0)                                          ? NB(get_index(row, col, depth - 1, w, d), ST)           : NB(-1, 0.0); // -Z
    nbrs[22] = (col + 1 < w && depth > 0)                           ? NB(get_index(row, col + 1, depth - 1, w, d), DG1)      : NB(-1, 0.0); // +X -Z
    nbrs[23] = (diag_ok && row + 1 < h && col > 0 && depth > 0)     ? NB(get_index(row + 1, col - 1, depth - 1, w, d), DG2)  : NB(-1, 0.0); // +Y -X -Z
    nbrs[24] = (row + 1 < h && depth > 0)                           ? NB(get_index(row + 1, col, depth - 1, w, d), DG1)      : NB(-1, 0.0); // +Y -Z
    nbrs[25] = (diag_ok && row + 1 < h && col + 1 < w && depth > 0) ? NB(get_index(row + 1, col + 1, depth - 1, w, d), DG2)  : NB(-1, 0.0); // +Y +X -Z


    // nbrs[9] = (depth - 1 > 0)                          ? NB(get_index(row, col, depth - 1, w, d), ST)        : NB(-1, 0.0); // - Z
    if(DEBUG) {
      printArray(nbrs, NUM_NEIGHBORS);  
    }

    float heuristic_cost;
    for (int i = 0; i < NUM_NEIGHBORS; ++i) {

      if (nbrs[i].idx >= 0) {
        int nbr_idx = nbrs[i].idx;
        int row_n = nbr_idx / (w *d);
        int col_n = (nbr_idx - (w * d * row_n)) / d;
        int depth_n = (nbr_idx - (w * d * row_n)) % d;
        if (DEBUG) {
          printf ("Inspecting neighbor idx: %d; (%d, %d, %d)\n", nbr_idx, row_n, col_n, depth_n);
        }
        // Dont expand neighbors that are obstacles
        if (weights[nbr_idx] > LARGE_NUMBER) {
          if (DEBUG) {
            printf("Skipping Obstacle Node: %.2f\n", weights[nbr_idx]);
          }
          continue;
        }
        // the sum of the cost so far and the cost of this move
                         // Parent Cost   // Risk Cost                  // Transition Cost
        float new_cost = costs[cur.idx] + RES * weights[nbr_idx] * W0 + RES * nbrs[i].m; // Need to change this
        if (DEBUG) {
          printf ("Current cost: %.2f; New cost %.2f \n", costs[nbr_idx], new_cost);
        }
        if (new_cost < costs[nbr_idx]) {
          // estimate the cost to the goal based on legal moves
          if (diag_ok) {
            heuristic_cost = octile(row_g, col_g, depth_g,
                                       row_n, col_n, depth_n);
          }
          else {
            heuristic_cost = l1_norm(row_g, col_g, depth_g,
                                     row_n, col_n, depth_n);
          }

          // paths with lower expected cost are explored first
          float priority = new_cost + heuristic_cost;
          if (DEBUG) {
            printf("Creating a new node for idx: %d; (%d, %d, %d). Priority = %.2f \n", nbr_idx, row_n, col_n, depth_n, priority);
          }
          nodes_to_visit.push(Node(nbr_idx, priority));

          costs[nbr_idx] = new_cost;
          paths[nbr_idx] = cur.idx;
        }
      }
    }
  }
  if (DEBUG) {
    printf("A-Star C++ Planner Finished\n");
  }
  delete[] costs;
  delete[] nbrs;

  if (solution_found) {
    if(DEBUG) {
      printf("Goal Node Solution: %.2f\n", goal_node.cost); 
    }
    return goal_node.cost;
  } else {
    return 0.0;
  }


  // return solution_found;
}
