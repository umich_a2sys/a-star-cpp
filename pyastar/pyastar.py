# distutils: language=c++
import sys
import os
from itertools import product
import ctypes
from glob import glob
import numpy as np


astar = None

### LOAD THE SHARED LIBRARY
def load_lib(lib_path=None):
    global astar
    library_path = 'astar.so'
    if lib_path:
        library_path = lib_path
    else: 
        THIS_DIR = os.path.dirname(__file__) # The directory of this file
        # This is absolutely wrong and ridiculous. I clearly know nothing about packaging
        # by bastardizing cython and setuptools
        dirs = [os.path.join(THIS_DIR + os.sep, '..' + os.sep), os.path.join(THIS_DIR + os.sep, '..' + os.sep, 'build' + os.sep ), os.path.join(THIS_DIR + os.sep,'..' + os.sep, 'build' + os.sep + '**' + os.sep)]
        extensions = ('.dll',) if sys.platform == 'win32' else ('.so',) 
        possible_shared_library = []
        for base_dir, ext in product(dirs, extensions):
            # print(base_dir + 'astar*' + ext)
            possible_shared_library.extend(glob(base_dir + 'astar*' + ext))

        if (len(possible_shared_library) == 0):
            print('ERROR PYASTAR: Couldnt find the shared library!  Cython should have built it, but something may have gone wrong.\n The source is included so just build it by hand, simple makefile included')
            print('File: {}'.format(os.path.abspath(__file__)))
            sys.exit(0)
        # print(possible_shared_library)
        
        print('Loading: {}'.format(possible_shared_library[0]))

        library_path = possible_shared_library[0]

    lib = ctypes.cdll.LoadLibrary(library_path)
    astar = lib.astar
    ndmat_f_type = np.ctypeslib.ndpointer(
        dtype=np.float32, ndim=1, flags='C_CONTIGUOUS')
    ndmat_i_type = np.ctypeslib.ndpointer(
        dtype=np.int32, ndim=1, flags='C_CONTIGUOUS')
    astar.restype = ctypes.c_float
    astar.argtypes = [ndmat_f_type, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                    ctypes.c_int, ctypes.c_int, ctypes.c_bool,
                    ndmat_i_type]

### END OF LOADING CPP SHARED LIBRARY

def astar_path(weights, start, goal, diag_ok=False, **kwargs):
    # assert weights.min(axis=None) >= 1., (
    #     'weights.min() = %.2f != 1' % weights.min(axis=None))
    # import time
    # t0 = time.time()
    rows, cols, depth = weights.shape
    start_idx = np.ravel_multi_index(start, (rows, cols, depth))

    goal_idx = np.ravel_multi_index(goal, (rows, cols, depth))

    flattened_weights = weights.ravel()

    paths = np.full(rows * cols * depth, -1, dtype=np.int32)
    # print(type(paths))
    # print(time.time() - t0)
    path_cost = astar(
        flattened_weights, rows, cols, depth, start_idx, goal_idx, diag_ok,
        paths  # output parameter
    )
    # print('Success: {}'.format(success))
    # print(path_cost)
    if path_cost == 0:
        return [], path_cost

    coordinates = []
    path_idx = goal_idx
    while path_idx != start_idx:
        pi, pj, pk = np.unravel_index(path_idx, (rows, cols, depth))
        coordinates.append([pi, pj, pk])

        path_idx = paths[path_idx]

    if coordinates:
        return np.vstack(coordinates[::-1]), path_cost
    else:
        return [], path_cost
