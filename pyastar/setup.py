from setuptools import setup, find_packages, Extension
# from Cython.Build import cythonize

module1 = Extension(
    'astar', ['astar.cpp']
)
setup(
    name='pyastar',
    packages=find_packages(),
    install_requires=['numpy', 'tabulate'],
    ext_modules=[module1])