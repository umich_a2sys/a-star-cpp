from os.path import join
from timeit import timeit

from pyastar import pyastar
import pymultiastar as pma
from util import create_nd_array, set_up_witten_environment
from tabulate import tabulate

NUMBER_BENCHMARK_EXECUTIONS = 10

MAZE_POSITIONS = [
    {'start': [0, 2, 0], 'goal': [1794, 1801, 0], 'name': 'Small Maze',
        'path_cost': '10031', 'filenames': [join('mazes', 'maze_small.png')]},
    # {'start': [0, 0, 0], 'goal': [3999, 4001, 0], 'name': 'Large Maze',
    #     'path_cost': '783736', 'filenames': [join('mazes', 'maze_large.png')]},
    {'start': [0, 0, 0], 'goal': [0, 31, 1], 'name': 'Small 3D Maze', 'path_cost': '67', 'filenames': [
        join('mazes', 'maze_small_3D_layer1.png'), join('mazes', 'maze_small_3D_layer2.png')]}
]

WITTEN_POSITIONS = [
    {'start': [155, 50, 8], 'goal': [155, 50, 9],
        'name': 'Witten Dead Simple', 'path_cost': '~2'},
    {'start': [155, 50, 8], 'goal': [116, 95, 12],
        'name': 'Witten Env Short', 'path_cost': '~135'},
    {'start': [155, 50, 8], 'goal': [56, 140, 9],
        'name': 'Witten Env Medium', 'path_cost': '~285'},
    {'start': [155, 50, 8], 'goal': [216, 308, 15],
        'name': 'Witten Env Long', 'path_cost': '~580'},
]

pyastar.load_lib()


def main():
    results = []
    # Maze Benchmarks (2D and 3D mazes)
    # print('Pyastar, Ctypes Call:')
    # for maze in MAZE_POSITIONS:
    #     # Get the grid file, and update keyword parameters for search
    #     grid = create_nd_array(maze['filenames'])
    #     maze.update({'weights': grid, 'diag_ok': False})

    #     path, path_cost = pyastar.astar_path(**maze)
    #     dur = timeit('pyastar.astar_path(**maze)',
    #                  number=NUMBER_BENCHMARK_EXECUTIONS, globals={**globals(), **locals()})
    #     results.append(
    #         [maze['name'], dur / NUMBER_BENCHMARK_EXECUTIONS, len(path), maze['path_cost']])

    # # 3D world environment Benchmarks
    # cost_map = set_up_witten_environment(join('mazes', 'cost_map.npy'))
    # for maze in WITTEN_POSITIONS:
    #     maze.update({'weights': cost_map, 'diag_ok': True})
    #     path, path_cost = pyastar.astar_path(**maze)
    #     dur = timeit('pyastar.astar_path(**maze)',
    #                  number=NUMBER_BENCHMARK_EXECUTIONS, globals={**globals(), **locals()})
    #     results.append(
    #         [maze['name'], dur / NUMBER_BENCHMARK_EXECUTIONS, path_cost, maze['path_cost']])

    # print(tabulate(results, headers=[
    #       'Name', 'Duration (s)', 'Path Cost', 'True Path Cost']))


    ##########################################################################
    print()
    print('Pymultiastar Class, C++ PyBind11 Bindings:')
    results = []
    # Maze Benchmarks (2D and 3D mazes)
    for maze in MAZE_POSITIONS:
        # Get the grid file, and update keyword parameters for search
        grid = create_nd_array(maze['filenames'])
        # print(grid.dtype, grid.shape)
        class_kwargs = {'map': grid, 'allow_diag': False}
        kwargs = {'start_cell': maze['start'], 'goal_cell': maze['goal']}
        astar_class = pma.PyMultiAStar(**class_kwargs)
        path, path_cost = astar_class.search_single(**kwargs)
        dur = timeit('astar_class.search_single(**kwargs)',
                     number=NUMBER_BENCHMARK_EXECUTIONS, globals={**globals(), **locals()})
        results.append(
            [maze['name'], dur / NUMBER_BENCHMARK_EXECUTIONS, len(path), maze['path_cost']])

    # 3D world environment Benchmarks
    cost_map = set_up_witten_environment(join('mazes', 'cost_map.npy'))
    for maze in WITTEN_POSITIONS:
        kwargs = {'map': cost_map, 'allow_diag': True,
                  'start_cell': maze['start'], 'goal_cells': [(maze['goal'], 1)]}
        class_kwargs = {'map': cost_map, 'allow_diag': True}
        kwargs = {'start_cell': maze['start'], 'goal_cell': maze['goal']}
        astar_class = pma.PyMultiAStar(**class_kwargs)
        path, path_cost = astar_class.search_single(**kwargs)
        dur = timeit('astar_class.search_single(**kwargs)',
                     number=NUMBER_BENCHMARK_EXECUTIONS, globals={**globals(), **locals()})
        results.append(
            [maze['name'], dur / NUMBER_BENCHMARK_EXECUTIONS, path_cost, maze['path_cost']])

    print(tabulate(results, headers=[
          'Name', 'Duration (s)', 'Path Cost', 'True Path Cost']))


if __name__ == '__main__':
    main()
