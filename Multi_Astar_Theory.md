What things would need to stay in the algorithm


All this stuff needs to be kept again, or persisted, on searching for the next goal:

  - The closed list
  - The priority Queue
  - The path map
  - The cost map

This stuff needs to be mutated, or changed

  - THe priority Queue order (heuristic has changed because the goal changed)


Questions

* What if you reach a different goal, goal b, while in pursuit of another one, goal a.
    * The goal check must look at all goals
    * If goal b is found, record it (meaning the path and its cost).  Then **continue** searching for the goal a, using the same priority queue (thats properly sorted for pursuit of goal a).
* What is the method signature for multi?

Pseudo Code

```
Make a new struct for goals: goal_struct
index
total_cost

Transform my vector of goal cells into goal structs - Calculate the estimated total cost of every goal node using the estimated path cost. 
Sort this vector of goal_structs by total_cost (estimated)


Then I need to sort my goal_cells using by calculating the total cost of the goal.  Use this by estimating the path_cost, and then doing a weighted sum
First I need to transform all my goal node cells into index hashes! Save as a new variable: goal_nodes

inline function find_best_estimated_goal(goals, i){
  go through list and find the best unfound goal
}

**goals should have a found bit flag!!!!**

best_planned_goal = Null
best_planned_goal_path - Null
vector found_goals = []
best_estimated_goal = Null

int unexpected_goal = 0

list_found_goals = []
for (i in goals.size) {
  <!-- goal = goals[i] -->
  path, path_cost, goal_index = search(&goals, i) // maybe change goals, !!!
  found_goal = goals[goal_index]
  found_goal.update_cost(path_cost) // updated cost and marks goal as found
  found_goals.push(goal)
  # Set the best found goal if it didnt exist or if this is better
  if (best_planned_goal is Null or found_goal.total_cost < best_planned_goal.total_cost ):
    best_planned_goal = found_goal
    best_planned_goal_path = path
  # Compare against the best estimated goal
  best_estimated_goal_cost = find_best_estimated_goal(goals, i)
  if (found_goal.total_cost <= min(best_planned_goal.total_cost, best_estimated_goal_cost)):
    # We are done! We have found a goal whos true planned total cost is less than any other possible goal
    return found_goal, path, path_cost

  # We need to keep searching!
  # Check if this was the goal that we were looking for originally
  # If it is NOT, we need to keep searching for the original goal
  if i == goal_index:
    i++;
  else:
    unexpected_goal++
  
}



function search(goals, i, sort=True) {
  primary_goal = goals[i]
  filtered_goals = copy_if(goals, goals.found is False)
  if (keep_nodes and sort) {
    resort priority queue
  } else {
    empty priroity queue and nodes
  }



}


```

I need to clean the goals first!!! Make sure that the goal is reachable!!!
Also give up after say, 100,000 expansions or so!!


cant keep the map alive in a member variable