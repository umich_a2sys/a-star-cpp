# A\* C++ Planner with Python Binding
This is a very simple C++ implementation of the A\* algorithm for pathfinding
on a three-dimensional grid. 

There are two modules in here

1. `pymultiastar` - New module that uses [pybind11](https://github.com/pybind/pybind11) to bind the c++ to a python module.
    1. The algorithm is also improved and significantly faster in my use cases (UAS aerial 3D path planning in urban cities)
    2. Can do single goal path planning or multi goal path planning.
2. `pyastar` - Old module that is basically forked from [here](https://github.com/hjweide/a-star) but extended to three dimensions.
    1. Uses `ctypes` to call a pure c++ function
    2. 3D (M x N x D) and 2D Planning (M x N x 1)
    3. Distutils distribution - you can find the old setup.py file that was used to compile it
    4. Only single goal path planning
    5. This module is inside the folder `pyastar`
    6. Uses the old examples.py file.

## Installing

### Pymultiastar

1. Install conda
2. `conda install -c conda-forge pybind11`
3. `python setup.py build install`


### Pyastar (old)

1. `cd pyastar`
2. `make` if on linux, else `cl.exe /LD pyastar\astar.cpp`
3. Copy the `.dll` or `.so` to the top level `build` folder. e.g `build/astar.dll`


## Multi-Goal Path Planning

Pymulitastar allows you to have the ability to search for **multiple** goals at the same time that have **different** values.  Think of these goals as **opportunities** in the world. These opportunities have different predetermined intrinsic **values** and have different unknown **search costs** in order to *find/plan to* them. In the problem specification you know their value and their specific location in the world, but you do **not** know to get to them a priori (optimal path from start to their position).  Your objective function is some combination of goal **value** as well as **path cost** to the goal, which is assumed to be a linear combination of the two which you provide.  Your **ultimate objective** is to *quickly* find the **one** goal/path combination that minimizes this objective function; but you **dont** know the true path cost (need for the objective function) until you actually plan to the goal. Path planning is expensive, so you would like to minimize planning to goals that are not likely minimize the objective.

Pymultistar can quickly find this answer for you. If you dont care about multiple goals or goal values and just want a single goal astar solver, pymultiastar can do this as well (that problem is just a subset of the one just framed).


## Benchmark

These benchmarks are for searching for a single goal.

Environments:

*  Small Maze - 1802 X 1802 ~ 3 Million
*  Large Maze - 4002 X 4002 ~ 16 Million
*  Small 3D Maze - 32 X 32 ~ 1024
*  Witten (Urban City) - 644 X 797 X 45 ~ 23 Million


Updated - Benchmarks on linux (Average 10 Runs)
```
Pyastar, Ctypes Call:
Name                  Duration (s)    Path Cost  True Path Cost
------------------  --------------  -----------  ----------------
Small Maze                0.532215    10031      10031
Large Maze                7.73623    783736      783736
Small 3D Maze             0.000965       67      67
Witten Dead Simple        0.149102        3.2    ~2
Witten Env Short          0.150002      135.008  ~135
Witten Env Medium         0.150797      285.043  ~285
Witten Env Long           0.171519      580.255  ~580

Pymultiastar Class, C++ Pybind11 Bindings:
Name                  Duration (s)    Path Cost  True Path Cost
------------------  --------------  -----------  ----------------
Small Maze               0.691967     10032      10031
Large Maze               4.14275     783737      783736
Small 3D Maze            0.000592        68      67
Witten Dead Simple       6.4e-06          3.2    ~2
Witten Env Short         0.003123       135.008  ~135
Witten Env Medium        0.001904       285.043  ~285
Witten Env Long          0.0280517      580.255  ~580

```

The `pyastar` module uses `ctypes` to call a foreign function (compiled directly from `pyastar/astar.cpp`)
Something interesting of note when running the A-Star planner in the Witten Environment. Initializing the `paths` array in python and the `cost` matrix in c++ each take about 50-80 ms.  So we need about 100-160 ms to just run the program, even if the goal node is **one** step away. 80 ms for 90 MB of memory.

The C++ bindings (pymultiastar) makes use of [pybind11](https://github.com/pybind/pybind11), which automatically binds c++ code to a python package. In addition the algorithm is a little different, it does **not** initialize the `cost` and `path` arrays, instead dynamically creating `costs` and `paths` using a hash map.  This saves on memory **and sometimes** time as well.  If the environment is very large and you have a pretty good heuristic (3D Witten environment) then it can be 10-20x faster.  Reasoning: it takes a long time to initialize very large regions of memory, in comparison to the speed of A* searching with a good heuristic.  If you have poor heuristic in a large search area (like a giant maze) then `pymulitastar` is the same or worse (1.25x slower (you are generating the memory anyways, because you searching everywhere, might as well do it in a efficient structure in the beginning)).  If you are in a small environment it doesn't really matter at all. My work is in **very** large 3D environments where we have pretty good heuristics hence I use dynamically generated node information.

Pymulitastar allows you to have the ability to search for **multiple** goals at the same time that have **different** values.  You can provide weighting tradeoffs between the path cost and the reward/risk of the goal.


## Notes

### Weight Data Structure

The weights are formatted as with dimensions (x,y,z) or (row, col, depth) and are C-Contiguous arrays! Therefore z has the fastest index, then y, then x. See this https://eli.thegreenplace.net/2015/memory-layout-of-multi-dimensional-arrays for more details.

## References
1. [A\* search algorithm on Wikipedia](https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode)
2. [Pathfinding with A* on Red Blob Games](http://www.redblobgames.com/pathfinding/a-star/introduction.html)

