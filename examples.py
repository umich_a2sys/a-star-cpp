# This module only tests an old version of the module named pyastar.
import numpy as np
# from matplotlib import pyplot as plt
from tabulate import tabulate
from pyastar import pyastar
import cv2

from time import time
from os.path import basename, join, splitext
import pdb
from util import create_nd_array, print_solution

DEBUG = False
THREE_D = 0

# IMPORTANT
# The images are formatted as with dimensions (x,y,z) or (row, col, depth) and 
# are C-Contiguous arrays! Therefore z has the fastest index, then y, then x
# See this https://eli.thegreenplace.net/2015/memory-layout-of-multi-dimensional-arrays

# INPUT FILES. TWO 2D MAZES STACKED ON TOP

MAZE_FPATH1 = join('mazes', 'maze_small_3D_layer1.png')
MAZE_FPATH2 = join('mazes', 'maze_small_3D_layer2.png')
MAZE_SMALL = join('mazes', 'maze_large.png')

FILE_NAMES = [MAZE_FPATH1, MAZE_FPATH2]
if not THREE_D:
    FILE_NAMES = [MAZE_SMALL]
    
print(FILE_NAMES)

def main():
    grid = create_nd_array(FILE_NAMES)

    # start is the first white block in the second row
    start_j, = np.where(grid[0, :, 0] == 1)
    start = np.array([0, start_j[0], 0])

    # end is the first white block in the final column
    end_i, = np.where(grid[:, -1, THREE_D] == 1)
    end = np.array([end_i[0], grid.shape[1] - 1, THREE_D])
    assert grid.min() == 1, 'cost of moving must be at least 1'

    print("Start Position: {}; End Position: {}".format( start, end))

    t0 = time()
    # set allow_diagonal=True to enable 8-connectivity
    pyastar.load_lib()
    path, path_cost = pyastar.astar_path(grid, start, end, diag_ok=False)
    dur = time() - t0

    if path.shape[0] > 0:
        print('found path of length %d in %.6fs' % (path.shape[0], dur))
        print_solution(path, FILE_NAMES)
        # maze[path[:, 0], path[:, 1]] = (0, 0, 255)

        # print('plotting path to %s' % (OUTP_FPATH))
        # cv2.imwrite(OUTP_FPATH, maze)
    else:
        print('no path found')

    print('done')

if __name__ == '__main__':
    main()
