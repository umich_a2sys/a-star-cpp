# Just testing if pmultiastar is working
import numpy as np
import pymultiastar as pmstar

# my_map = np.array([[[ 0.,  0. , 0.],[ 0.,  0.,  0.],[ 0.,  0.,  0.]],[[ 0.,  0.,  0.],[ 0.,  0.,  0.],[ 0. , 0. , 0.]],[[ 0.,  0. , 0.],[ 0.,  0. , 0.],[ 0.,  0. , 0.]]], dtype='f4')
my_map = np.zeros(shape=(3,3,3),dtype='f4')

my_map[1,1,1] = 0 # Add obstacle
my_map[1,1,0] = 1.5 # Add obstacle
start_cell = [0,0,0]
goal_cells = [([0,0,1], 6), ([2,2,2], 1), ([2,2,1], 4)]

# Planner things ([2,2,2], 1) Will be the lowest cost risk, but the obstacle in the center ([1,1,1]) will make it be wrong
# It will then find ([0,0,1], 6) to be the best path/value pair

stuff = pmstar.PyMultiAStar(my_map, keep_nodes=True)
print(stuff)
print(stuff.search_multiple(start_cell, goal_cells))

print(stuff.search_single(start_cell, goal_cells[1][0]))
