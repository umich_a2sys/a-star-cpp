from os.path import join, splitext, basename
import numpy as np
from tabulate import tabulate
import cv2


def create_nd_array(filenames, three_d=True, debug=False):
    imgs = []
    for fname in filenames:
        im = cv2.imread(fname)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY).astype(np.float32)
        imgs.append(im)

    maze = np.ascontiguousarray(np.stack(imgs, axis=2))
    maze[maze == 0] = np.inf
    maze[maze == 255] = 0
    if debug:
        print(maze.shape, maze.dtype)
        print(tabulate(maze[:,:, 0]))
        if three_d:
            print(tabulate(maze[:,:, 1]))

    return maze

def print_solution(path, fnames):
    for i, fname in enumerate(fnames):
        # Get the image and path components on the image
        maze = cv2.imread(fname)
        mask = path[:, 2] == i
        if mask.shape[0] == 0:
            continue
        path_ = path[mask]
        # Mark the path red
        maze[path_[:, 0], path_[:, 1]] = (0, 0, 255)
        out_path = join('solns', '%s_soln.png' % splitext(basename(fname))[0])
        cv2.imwrite(out_path, maze)
        print(path)

def set_up_witten_environment(filename):
    cost_map = np.load(filename)
    cost_map = cost_map / 255.0
    cost_map[cost_map == 1.0] = np.inf
    return cost_map